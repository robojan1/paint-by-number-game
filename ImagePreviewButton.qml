import QtQuick 2.0

Rectangle {

    property var info

    color: "lightgray"
    radius: 10

    signal clicked(var mouse)

    Image {
        id: imagesListPreviewImage
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.topMargin: 5
        anchors.bottomMargin: 5

        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "image://imgdb/preview/" + info.uuid.toString()
        cache: true
    }

    Rectangle {
        color: "black"
        height: 30
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        opacity: 0.6
        radius: 10

        Text {
            color: "white"
            id: imagesListPreviewName
            text: info.name
            anchors.fill: parent
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: parent.clicked(mouse)
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:100;width:100}
}
##^##*/
