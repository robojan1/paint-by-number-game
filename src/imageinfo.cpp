#include "imageinfo.h"
#include <QJsonDocument>
#include <QFile>
#include <QDir>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

ImageInfo::ImageInfo(QString name, QString previewImage, QString pixelatedImage, QUuid uuid, QObject *parent) : QObject(parent),
    _name(std::move(name)), _uuid(std::move(uuid)), _previewImagePath(previewImage), _pixelatedImagePath(pixelatedImage)
{

}

ImageInfo::ImageInfo(QString name, QImage previewImage, PixelatedImage pixelatedImage, QUuid uuid, QObject *parent) : QObject(parent),
    _name(std::move(name)), _uuid(std::move(uuid)), _loadedPreviewImage(std::move(previewImage)), _loadedPixelatedImage(std::move(pixelatedImage))
{

}

ImageInfo *ImageInfo::fromDir(const QString &dir, QObject *parent)
{
    QString infoFilePath = dir + QDir::separator() + QStringLiteral("info.json");
    QString previewFile = dir + QDir::separator() + QStringLiteral("preview.png");
    QString pixelatedFile = dir + QDir::separator() + QStringLiteral("image.pim");

    if(!QFileInfo::exists(infoFilePath) || !QFileInfo::exists(previewFile) || !QFileInfo::exists(pixelatedFile)) {
        qWarning() << qPrintable(tr("Image \"%1\" is missing some files").arg(dir));
        return nullptr;
    }

    QFile infoFile(infoFilePath);
    if(!infoFile.open(QIODevice::ReadOnly)) {
        qWarning() << qPrintable(tr("Could not read image info: %1").arg(infoFile.errorString()));
        return nullptr;
    }
    QByteArray infoData = infoFile.readAll();

    QJsonParseError parseError;
    QJsonDocument infoDoc(QJsonDocument::fromJson(infoData, &parseError));
    if(parseError.error != QJsonParseError::NoError) {
        qWarning() << qPrintable(tr("Image info format error:\n%1").arg(parseError.errorString()));
        return nullptr;
    }

    if(!infoDoc.isObject()) {
        qWarning() << qPrintable(tr("Image info should be a json object"));
        return nullptr;
    }

    auto obj = infoDoc.object();
    if(!obj.contains(QStringLiteral("id")) || !obj.contains(QStringLiteral("name"))) {
        qWarning() << qPrintable(tr("Missing required sections in the image info"));
        return nullptr;
    }

    auto idObj = obj[QStringLiteral("id")];
    if(!idObj.isString()) {
        qWarning() << qPrintable(tr("id should be of type string in the image info"));
        return nullptr;
    }
    auto nameObj = obj[QStringLiteral("name")];
    if(!nameObj.isString()) {
        qWarning() << qPrintable(tr("name should be of type string in the image info"));
        return nullptr;
    }

    QString id = idObj.toString();
    QString name = nameObj.toString();

    auto result = new ImageInfo(name, previewFile, pixelatedFile, QUuid::fromString(id), parent);
    return result;
}

void ImageInfo::save(const QString &dirPath)
{
    if(!isValid()) return;

    // Create the directories if they do not exist
    QDir dir(dirPath);
    if(!dir.mkpath(".")) {
        qWarning() << qPrintable(tr("Could not create the image database directory."));
        return;
    }

    // Copy the images to the directory
    QString previewFile = dirPath + QDir::separator() + QStringLiteral("preview.png");
    QString pixelatedFile = dirPath + QDir::separator() + QStringLiteral("image.pim");
    if(_loadedPreviewImage.isNull()) {
        Q_ASSERT(!_previewImagePath.isEmpty());
        _loadedPreviewImage = QImage(_previewImagePath);
        if(_loadedPreviewImage.isNull()) {
            qWarning() << qPrintable(tr("Error loading image"));
            dir.removeRecursively();
            return;
        }
    }
    if(!_loadedPreviewImage.save(previewFile)) {
        qWarning() << qPrintable(tr("Could not copy the preview image."));
        return;
    }
    _previewImagePath = previewFile;
    if(!_loadedPixelatedImage.isValid()) {
        Q_ASSERT(!_pixelatedImagePath.isEmpty());
        _loadedPixelatedImage = PixelatedImage::fromFile(_pixelatedImagePath);
        if(!_loadedPixelatedImage.isValid()) {
            qWarning() << qPrintable(tr("Error loading pixelated image"));
            dir.removeRecursively();
            return;
        }
    }
    if(!_loadedPixelatedImage.save(pixelatedFile)) {
        qWarning() << qPrintable(tr("Could not save the pixelated image."));
        return;
    }
    _pixelatedImagePath = pixelatedFile;

    // Create the JSON object
    QJsonObject mainObject;
    mainObject.insert(QStringLiteral("id"), QJsonValue::fromVariant(_uuid.toString()));
    mainObject.insert(QStringLiteral("name"), QJsonValue::fromVariant(_name));

    QJsonDocument infoDoc(mainObject);
    auto infoData = infoDoc.toJson(QJsonDocument::Indented);

    // Write to the file
    auto infoFilePath = dirPath + QDir::separator() + QStringLiteral("info.json");
    QFile infoFile(infoFilePath);
    if(!infoFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qWarning() << qPrintable(tr("Could not open image info file for writing: %1").arg(infoFile.errorString()));
        return;
    }
    qint64 written = 0;
    while(written < infoData.size()) {
        qint64 toWrite = infoData.size() - written;
        qint64 actuallyWritten = infoFile.write(infoData.data() + written, toWrite);
        if(actuallyWritten < 0) {
            qWarning() << qPrintable(tr("Error writing to the image database: %1").arg(infoFile.errorString()));
            return;
        }
        written += actuallyWritten;
    }
}

QImage ImageInfo::previewImage(bool cache)
{
    if(!_loadedPreviewImage.isNull()) {
        return _loadedPreviewImage;
    }
    Q_ASSERT(!_previewImagePath.isEmpty());
    auto img = QImage(_previewImagePath);
    if(img.isNull()) {
        qWarning() << qPrintable(tr("Error loading pixelated image"));
        return img;
    }
    if(cache) {
        _loadedPreviewImage = img;
    }
    return img;
}

PixelatedImage ImageInfo::pixelatedImage(bool cache)
{
    if(_loadedPixelatedImage.isValid()) {
        return _loadedPixelatedImage;
    }
    Q_ASSERT(!_pixelatedImagePath.isEmpty());
    auto img = PixelatedImage::fromFile(_pixelatedImagePath);
    if(!img.isValid()) {
        qWarning() << qPrintable(tr("Error loading pixelated image"));
        return img;
    }
    if(cache) {
        _loadedPixelatedImage = img;
    }
    return img;
}
