#include "standardpaths.h"
#include <QStandardPaths>
#include <QDir>

StandardPaths *StandardPaths::_instance;

StandardPaths::StandardPaths(QObject *parent) : QObject(parent)
{

}

QString StandardPaths::dataDir() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString StandardPaths::imageDatabaseDir() const
{
    return dataDir() + QDir::separator() + "database";
}
