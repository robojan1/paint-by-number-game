#include "downloadmanager.h"
#include <QTemporaryFile>
#include <QFile>
#include <QFutureInterface>
#include <QNetworkReply>
#include <QDir>

DownloadManager *DownloadManager::_instance = nullptr;

DownloadManager::DownloadManager(QObject *parent) : QObject(parent), _nm(new QNetworkAccessManager(this))
{

}

QFuture<DownloadResult> DownloadManager::download(QUrl url)
{
    QString ext = QFileInfo(url.fileName()).completeSuffix();
    QString tempName = QDir::tempPath() + QDir::separator() + "XXXXXX." + ext;

    QTemporaryFile *file = new QTemporaryFile(tempName);
    file->setAutoRemove(false);
    if(!file->isOpen()) {
       file->open();
    }
    return download(url, file->fileName(), file, true, true);
}

QFuture<DownloadResult> DownloadManager::download(QUrl url, QString outputFile)
{
    QFile *file = new QFile(outputFile);
    if(!file->isOpen()) {
        file->open(QIODevice::ReadWrite);
    }
    return download(url, outputFile, file, true, true);
}

QFuture<DownloadResult> DownloadManager::download(QUrl url, QString path, QIODevice *outputFile, bool autoClose, bool deleteOnFail)
{
    QFutureInterface<DownloadResult> futureInterface;
    futureInterface.reportStarted();
    if(!outputFile->isWritable()) {
        // Error
        futureInterface.reportResult(DownloadResult(false, tr("Could not open output file")));
        return futureInterface.future();
    }

    QNetworkRequest request(url);
    auto reply = _nm->get(request);
    connect(reply, &QNetworkReply::finished, this, &DownloadManager::onRequestFinished);
    connect(reply, &QNetworkReply::downloadProgress, this, &DownloadManager::onRequestProgress);
    connect(reply, qOverload<QNetworkReply::NetworkError>(&QNetworkReply::error), this, &DownloadManager::onRequestError);
    connect(reply, &QNetworkReply::readyRead, this, &DownloadManager::onRequestReadyRead);

    DownloadStatus status;
    status.file = outputFile;
    status.reply = reply;
    status.futureInterface = futureInterface;
    status.path = path;
    status.autoClose = autoClose;
    status.deleteOnFail = deleteOnFail;
    status.failed = false;
    _status.insert(reply, std::move(status));

    return futureInterface.future();
}

bool DownloadManager::writeAvailableData(DownloadStatus *status)
{
    QByteArray buffer(4096, 0);
    while(true) {
        qint64 dataRead = status->reply->read(buffer.data(), buffer.size());
        if(dataRead <= 0) break;
        qint64 totalWritten = 0;
        while(totalWritten < dataRead) {
            qint64 written = status->file->write(buffer.data() + totalWritten, dataRead - totalWritten);
            if(written < 0) {
                return false;
            }
            totalWritten += written;
        }
    }
    return true;
}

void DownloadManager::onRequestFinished()
{
    auto sender = qobject_cast<QNetworkReply *>(QObject::sender());
    auto &status = _status[sender];


    if(sender->error() == QNetworkReply::NoError) {
        bool successfulyWritten = writeAvailableData(&status);
        if(successfulyWritten) {
            DownloadResult result(true, status.path);
            auto contentType = sender->header(QNetworkRequest::ContentTypeHeader);
            if(contentType.isValid() && !contentType.isNull()) {
                result.setMimeType(contentType.toString());
            }
            status.futureInterface.reportResult(result);
        } else {
            status.futureInterface.reportResult(DownloadResult(false, tr("Could not write to file")));
            status.failed = true;
        }
    }

    // Close file
    if(status.autoClose) {
        status.file->close();
        status.file->deleteLater();
    }
    if(status.failed && status.deleteOnFail && !status.path.isEmpty())
    {
        QFile::remove(status.path);
    }

    status.futureInterface.reportFinished();

    _status.remove(sender);
}

void DownloadManager::onRequestProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    auto sender = qobject_cast<QNetworkReply *>(QObject::sender());
    auto &status = _status[sender];
    status.futureInterface.setProgressRange(0, static_cast<int>(bytesTotal));
    status.futureInterface.setProgressValue(static_cast<int>(bytesReceived));
}

void DownloadManager::onRequestError(QNetworkReply::NetworkError code)
{
    Q_UNUSED(code)
    auto sender = qobject_cast<QNetworkReply *>(QObject::sender());
    auto &status = _status[sender];
    DownloadResult result(false, sender->errorString());
    status.futureInterface.reportResult(result);
}

void DownloadManager::onRequestReadyRead()
{
    auto sender = qobject_cast<QNetworkReply *>(QObject::sender());
    auto &status = _status[sender];

    bool successfulyWritten = writeAvailableData(&status);
    if(!successfulyWritten) {
        status.futureInterface.reportResult(DownloadResult(false, tr("Could not write to file")));
        sender->abort();
        status.failed = true;
    }
}
