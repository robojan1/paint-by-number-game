#include "appstate.h"
#include "imagedatabaseimageprovider.h"

void AppState::registerQml()
{
    qmlRegisterUncreatableType<AppState>("App", 1, 0, "AppState", tr("The state is an internal object"));


    AppEditorState::registerQml();
}

AppState::AppState(QObject *parent) : QObject(parent), _editorState(new AppEditorState(this))
{
    connect(_editorState, &AppEditorState::changed, this, &AppState::editorStateChanged);
    connect(&_imgDb, &QAbstractItemModel::dataChanged, this, &AppState::imageDatabaseChanged);

    _imgDb.loadDatabase();

}

AppState::~AppState()
{
    _imgDb.saveDatabase();
}

QQuickImageProvider *AppState::imageDatabaseImageProvider() const
{
    return new ImageDatabaseImageProvider(&_imgDb);
}
