#ifndef PIXELATEDIMAGE_H
#define PIXELATEDIMAGE_H

#include <QColor>
#include <QSize>
#include <QImage>
#include <QPoint>

#include <stdint.h>
#include <vector>
#include <unordered_map>

class PixelatedImage
{
    std::vector<int> _data;
    std::vector<QRgb> _colors;
    QSize _size;

public:
    PixelatedImage() : _size(0,0) {}
    PixelatedImage(const QImage &image, int pixelWidth, int pixelHeight);
    ~PixelatedImage() = default;

    const std::vector<QRgb> colors() const { return _colors; }
    const std::vector<int> data() const { return _data; }

    bool isValid() const { return _size.isValid() && _data.size() > 0; }

    QImage asImage() const;
    QColor getPixelColor(int x, int y) const;
    QColor getPixelColor(QPoint p) const { return getPixelColor(p.x(), p.y()); }
    int getPixelIdx(int x, int y) const;
    int getPixelIdx(QPoint p) const { return getPixelIdx(p.x(), p.y()); }

    QSize size() const { return _size; }

    void reduce(int numColors);

    static PixelatedImage fromFile(const QString &path);
    bool save(const QString &path) const;

private:
    explicit PixelatedImage(decltype (_data) data, decltype(_colors) colors, decltype (_size) size);

};

#endif // PIXELATEDIMAGE_H
