#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QObject>
#include <QUuid>
#include <QImage>
#include "pixelatedimage.h"

class ImageInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QUuid uuid READ uuid WRITE setUuid NOTIFY uuidChanged)
    Q_PROPERTY(QString previewImagePath READ previewImagePath WRITE setPreviewImagePath NOTIFY previewImagePathChanged)
    Q_PROPERTY(QString pixelatedImagePath READ pixelatedImagePath WRITE setPixelatedImagePath NOTIFY pixelatedImagePathChanged)

    QString _name;
    QUuid _uuid;

    QString _previewImagePath;
    QString _pixelatedImagePath;
    QImage _loadedPreviewImage;
    PixelatedImage _loadedPixelatedImage;
public:
    explicit ImageInfo(QString name, QString previewImage, QString pixelatedImage, QUuid uuid = QUuid::createUuid(), QObject *parent = nullptr);
    explicit ImageInfo(QString name, QImage previewImage, PixelatedImage pixelatedImage, QUuid uuid = QUuid::createUuid(), QObject *parent = nullptr);
    explicit ImageInfo() = default;

    static ImageInfo *fromDir(const QString &dir, QObject *parent = nullptr);

    void save(const QString &dir);

    QString name() const { return _name; }
    QUuid uuid() const { return _uuid; }
    QString previewImagePath() const { return _previewImagePath; }
    QString pixelatedImagePath() const { return _pixelatedImagePath; }

    QImage previewImage(bool cache = true);
    PixelatedImage pixelatedImage(bool cache = true);

    bool isValid() const {
        return !_name.isEmpty() &&
               (!_previewImagePath.isEmpty() || !_loadedPreviewImage.isNull()) &&
               (!_pixelatedImagePath.isEmpty() || _loadedPixelatedImage.isValid());
    }

    void setName(QString name) {
        if(_name != name) {
            _name = std::move(name);
            emit nameChanged();
        }
    }
    void setUuid(QUuid uuid) {
        if(_uuid != uuid) {
            _uuid = std::move(uuid);
            emit uuidChanged();
        }
    }
    void setPreviewImagePath(QString path) {
        if(_previewImagePath != path) {
            _previewImagePath = std::move(path);
            emit previewImagePathChanged();
        }
    }
    void setPixelatedImagePath(QString path) {
        if(_pixelatedImagePath != path) {
            _pixelatedImagePath = std::move(path);
            emit pixelatedImagePathChanged();
        }
    }


signals:
    void nameChanged();
    void uuidChanged();
    void previewImagePathChanged();
    void pixelatedImagePathChanged();

};

Q_DECLARE_METATYPE(ImageInfo *)

#endif // IMAGEINFO_H
