#ifndef APPEDITORSTATE_H
#define APPEDITORSTATE_H

#include <QObject>
#include <QImage>
#include <QQuickImageProvider>
#include <QFutureWatcher>
#include "downloadmanager.h"
#include "pixelatedimage.h"

class AppState;

class AppEditorState : public QObject, public QQuickImageProvider
{
    Q_OBJECT
    Q_ENUMS(LoadingState)
    Q_PROPERTY(PixelatedImage *pixelatedImage READ pixelatedImage NOTIFY pixelatedImageChanged)
    Q_PROPERTY(bool imageLoaded READ imageLoaded NOTIFY activeImageChanged)
    Q_PROPERTY(bool imageGenerated READ imageGenerated NOTIFY pixelatedImageChanged)

    QImage _activeImage;
    PixelatedImage _pixelatedImage;
    QFutureWatcher<DownloadResult> _downloadWatcher;
    AppState *_appState;
public:    
    enum LoadingState {
        Loading,
        Failed,
        Processing,
        Done,
    };

    static void registerQml();

    explicit AppEditorState(AppState *parent);

    QImage activeImage() { return _activeImage; }
    PixelatedImage *pixelatedImage() { return &_pixelatedImage; }
    bool imageLoaded() const { return !_activeImage.isNull(); }
    bool imageGenerated() const { return  _pixelatedImage.isValid(); }

    void setActiveImage(QImage image);
    Q_INVOKABLE void loadImage(QUrl url);
    Q_INVOKABLE void loadImageFromFile(QString path, QString mimeType = QString());
    Q_INVOKABLE void loadImageFromDB(QString id);

    Q_INVOKABLE void generatePixelatedImage(int pixelSize, int numColors);

    Q_INVOKABLE void saveImage(QString name);

    Q_INVOKABLE void removeFromDB(QString id);

    // QQmlImageProviderBase interface
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;

signals:
    void activeImageChanged();
    void pixelatedImageChanged();
    void imagesLoaded();
    void changed();
    void imageLoadingProgress(LoadingState state, float progress, QString message);

private slots:
    void onFileDownloaded();
    void onFileDownloadProgress(int value);

private:
    void generatePixelatedImageInBackground(int pixelSize, int numColors);
};

#endif // APPEDITORSTATE_H
