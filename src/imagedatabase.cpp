#include "imagedatabase.h"
#include <QVariant>
#include <standardpaths.h>
#include <QDir>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrl>

ImageDatabase::ImageDatabase()
{
}

ImageDatabase::~ImageDatabase()
{
    for(auto ptr : _data) {
        if(ptr->parent() == nullptr) {
            ptr->deleteLater();
        }
    }
}

void ImageDatabase::loadDatabase()
{
    auto dirPath = StandardPaths::instance()->imageDatabaseDir();
    auto dbFilePath = dirPath + QDir::separator() + "db.json";

    if(!QFileInfo::exists(dbFilePath)) {
        qInfo() << qPrintable(tr("No image database available"));
        return;
    }

    QFile dbFile(dbFilePath);
    if(!dbFile.open(QIODevice::ReadOnly)) {
        qWarning() << qPrintable(tr("Could not read image database: %1").arg(dbFile.errorString()));
        return;
    }
    QByteArray dbData = dbFile.readAll();

    QJsonParseError parseError;
    QJsonDocument dbDoc(QJsonDocument::fromJson(dbData, &parseError));
    if(parseError.error != QJsonParseError::NoError) {
        qWarning() << qPrintable(tr("Image database format error:\n%1").arg(parseError.errorString()));
        return;
    }

    if(!dbDoc.isObject()) {
        qWarning() << qPrintable(tr("Image database should be a json object"));
        return;
    }

    auto obj = dbDoc.object();
    if(!obj.contains(QStringLiteral("images")))
    {
        qWarning() << qPrintable(tr("Missing required sections in the image database"));
        return;
    }

    auto imagesObj = obj[QStringLiteral("images")];
    if(!imagesObj.isArray()) {
        qWarning() << qPrintable(tr("Invalid object type in image database"));
        return;
    }

    loadImages(imagesObj.toArray());


}

void ImageDatabase::loadImages(const QJsonArray &list)
{
    auto dirPath = StandardPaths::instance()->imageDatabaseDir() + QDir::separator();
    for(auto item : list) {
        if(!item.isString()) {
            qWarning() << qPrintable(tr("Image is of the wrong type in the image database"));
            continue;
        }
        auto uuidStr = item.toString();
        auto imageDir = dirPath + uuidStr;
        auto info = ImageInfo::fromDir(imageDir, this);
        if(info != nullptr) {
            _data.push_back(info);
            _mapping[info->uuid().toString()] = info;
        }
    }
}

void ImageDatabase::saveImages(QJsonArray &list)
{
    auto dirPath = StandardPaths::instance()->imageDatabaseDir() + QDir::separator();
    for(auto image : _data)
    {
        auto uuidStr = image->uuid().toString();
        auto imageDir = dirPath + uuidStr + QDir::separator();
        if(!QFileInfo::exists(imageDir)) {
            image->save(imageDir);
        }
        list.push_back(QJsonValue::fromVariant(uuidStr));
    }
}

void ImageDatabase::saveDatabase()
{
    // Create the JSON object
    QJsonArray imagesArray;
    saveImages(imagesArray);

    QJsonObject mainObject;
    mainObject.insert(QStringLiteral("images"), imagesArray);

    QJsonDocument dbDoc(mainObject);
    auto dbData = dbDoc.toJson(QJsonDocument::Indented);

    // Create the directories if they do not exist
    auto dirPath = StandardPaths::instance()->imageDatabaseDir();
    QDir dir(dirPath);
    if(!dir.mkpath(".")) {
        qWarning() << qPrintable(tr("Could not create the image database directory."));
        return;
    }

    // Write to the file
    auto dbFilePath = dirPath + QDir::separator() + "db.json";
    QFile dbFile(dbFilePath);
    if(!dbFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qWarning() << qPrintable(tr("Could not open image database file for writing: %1").arg(dbFile.errorString()));
        return;
    }
    qint64 written = 0;
    while(written < dbData.size()) {
        qint64 toWrite = dbData.size() - written;
        qint64 actuallyWritten = dbFile.write(dbData.data() + written, toWrite);
        if(actuallyWritten < 0) {
            qWarning() << qPrintable(tr("Error writing to the image database: %1").arg(dbFile.errorString()));
            return;
        }
        written += actuallyWritten;
    }
}

int ImageDatabase::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid()) return 0;
    return _data.size();
}

QVariant ImageDatabase::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());

    int idx = index.row();
    if(idx < 0 || idx >= _data.size()) return QVariant();

    auto element = _data[idx];

    switch(role) {
    case Qt::DisplayRole:
        return element->name();
    case DataRole:
        return QVariant::fromValue(element);
    default: return QVariant();
    }
}

QHash<int, QByteArray> ImageDatabase::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DataRole] = "data";
    return roles;
}

void ImageDatabase::push_back(ImageInfo * info)
{
    Q_ASSERT(info != nullptr);
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    _data.push_back(info);
    _mapping[info->uuid().toString()] = info;

    endInsertRows();
}

void ImageDatabase::remove(QString id)
{
    auto infoIt = _mapping.find(id);
    if(infoIt == _mapping.end()) {
        return;
    }

    auto info = infoIt.value();

    auto dataIt = _data.indexOf(info);
    Q_ASSERT(dataIt >= 0);

    beginRemoveRows(QModelIndex(), dataIt, dataIt);

    _mapping.erase(infoIt);
    _data.erase(_data.begin()+dataIt);

    endRemoveRows();
    if(info->parent() == nullptr) {
        info->deleteLater();
    }
}

ImageInfo *ImageDatabase::operator[](const QString &id)
{
    auto infoIt = _mapping.find(id);
    if(infoIt == _mapping.end()) {
        return nullptr;
    }
    return infoIt.value();
}

QImage ImageDatabase::requestImage(const QString &id, QSize *size, const QSize &requestedSize) const
{
    auto decodedId = QUrl::fromPercentEncoding(id.toUtf8());
    auto idParts = decodedId.splitRef('/');
    if(idParts.size() != 2) {
        return QImage();
    }
    auto &typeId = idParts[0];

    auto infoIt = _mapping.find(idParts[1].toString());
    if(infoIt == _mapping.end()) {
        return QImage();
    }

    auto info = infoIt.value();

    QImage srcImg;
    Qt::TransformationMode scalingMode;
    if(typeId == "pixelated") {
        srcImg = info->pixelatedImage(false).asImage();
        scalingMode = Qt::FastTransformation;
    } else if(typeId == "preview") {
        srcImg = info->previewImage(false);
        scalingMode = Qt::SmoothTransformation;
    } else {
        return QImage();
    }

    if(size) {
        *size = srcImg.size();
    }
    if(requestedSize.isValid()) {
        srcImg.scaled(requestedSize, Qt::KeepAspectRatio, scalingMode);
    }
    return srcImg;
}
