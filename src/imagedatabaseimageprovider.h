#ifndef IMAGEDATABASEIMAGEPROVIDER_H
#define IMAGEDATABASEIMAGEPROVIDER_H

#include <QObject>
#include <QQuickImageProvider>

class ImageDatabase;

class ImageDatabaseImageProvider : public QQuickImageProvider
{
    const ImageDatabase *_db;
public:
    ImageDatabaseImageProvider(const ImageDatabase *imgdb);

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
};

#endif // IMAGEDATABASEIMAGEPROVIDER_H
