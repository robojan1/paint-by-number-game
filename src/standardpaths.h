#ifndef STANDARDPATHS_H
#define STANDARDPATHS_H

#include <QObject>

class StandardPaths : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString dataDir READ dataDir)
    Q_PROPERTY(QString imageDatabaseDir READ imageDatabaseDir)

    static StandardPaths *_instance;
public:
    static StandardPaths *instance() {
        if(_instance == nullptr) {
            _instance = new StandardPaths();
        }
        return _instance;
    }

    QString dataDir() const;
    QString imageDatabaseDir() const;

signals:


private:
    StandardPaths(QObject *parent = nullptr);
};

#endif // STANDARDPATHS_H
