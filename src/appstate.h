#ifndef APPSTATE_H
#define APPSTATE_H

#include <QObject>
#include <appeditorstate.h>
#include "imagedatabase.h"

class AppState : public QObject
{
    Q_OBJECT
    Q_PROPERTY(AppEditorState *editorState READ editorState NOTIFY editorStateChanged)
    Q_PROPERTY(ImageDatabase *imageDatabase READ imageDatabase NOTIFY imageDatabaseChanged)

    AppEditorState *_editorState;
    ImageDatabase _imgDb;
public:
    static void registerQml();

    explicit AppState(QObject *parent = nullptr);
    ~AppState();

    AppEditorState *editorState() { return _editorState; }
    ImageDatabase *imageDatabase() { return &_imgDb; }
    QQuickImageProvider *imageDatabaseImageProvider() const;

signals:
    void editorStateChanged();
    void imageDatabaseChanged();

};

#endif // APPSTATE_H
