#include "pixelatedimage.h"
#include <QColorSpace>
#include <QGenericMatrix>
#include <math.h>
#include <set>
#include <algorithm>
#include <execution>
#include <QRandomGenerator>
#include <QFile>

using QMatrix1x3 = QGenericMatrix<1,3,float>;

static QMatrix1x3 sRGBtoXYZ(const QMatrix1x3 rgb)
{
    static const float srgbToXYZValues[9] = {0.4124564f, 0.3575761f, 0.1804375f,
                                             0.2126729f, 0.7151522f, 0.0721750f,
                                             0.0193339f, 0.1191920f, 0.9503041f};
    static const QMatrix3x3 srgbToXYZ{srgbToXYZValues};

    QMatrix1x3 grgb;

    auto gcor = [](float x) -> float {return x > 0.04045f ? std::pow((x + 0.055f)/1.055f, 2.4f) : (x / 12.92f); };

    grgb(0,0) = gcor(rgb(0,0));
    grgb(1,0) = gcor(rgb(1,0));
    grgb(2,0) = gcor(rgb(2,0));

    return srgbToXYZ * grgb;
}

static QMatrix1x3 XYZtoLab(const QMatrix1x3 &xyz)
{
    static const float xyzN[3] = {0.95089f, 1.0f, 1.088840f};
    static const float xyzNormValues[9] = {1/xyzN[0], 0, 0,
                                             0, 1/xyzN[1], 0,
                                             0, 0, 1/xyzN[2]};
    const QMatrix3x3 xyzNormalization{xyzNormValues};

    auto f = [](float t){
        float delta = 6.0f/29.0f;
        if(t > delta * delta * delta)
        {
            return std::cbrt(t);
        } else {
            return t / (3 * delta * delta) + 4.0f/29.0f;
        }
    };

    auto xyzNorm = xyzNormalization * xyz;
    QGenericMatrix<1,3,float> labVector;
    labVector(0,0) = 116.0f * f(xyzNorm(1,0)) - 16;
    labVector(1,0) = 500.0f * (f(xyzNorm(0,0)) - f(xyzNorm(1,0)));
    labVector(2,0) = 200.0f * (f(xyzNorm(1,0)) - f(xyzNorm(2,0)));

    return labVector;
}

static QMatrix1x3 sRGBtoLab(const QMatrix1x3 &rgb)
{
    return XYZtoLab(sRGBtoXYZ(rgb));
}

static QMatrix1x3 sRGBtoLab(QRgb rgb)
{
    QMatrix1x3 rgbVec;
    rgbVec(0,0) = qRed(rgb)/255.0f;
    rgbVec(1,0) = qGreen(rgb)/255.0f;
    rgbVec(2,0) = qBlue(rgb)/255.0f;
    QMatrix1x3 res = XYZtoLab(sRGBtoXYZ(rgbVec));
    Q_ASSERT(!std::isnan(res(0,0)) && !std::isnan(res(1,0)) && !std::isnan(res(2,0)));
    return res;
}

static QMatrix1x3 LabToXYZ(const QMatrix1x3 &Lab)
{
    static const float xyzN[3] = {0.95089f, 1.0f, 1.088840f};
    auto finv = [](float t){
        float delta = 6.0f/29.0f;
        if(t > delta)
        {
            return t * t * t;
        } else {
            return 3 * t * t * (t - 4.0f/29.0f);
        }
    };
    float L = Lab(0,0);
    float a = Lab(1,0);
    float b = Lab(2,0);

    QGenericMatrix<1,3,float> xyzVector;
    xyzVector(0,0) = xyzN[0] * finv((L + 16)/116 + a /500);
    xyzVector(1,0) = xyzN[1] * finv((L + 16)/116);
    xyzVector(2,0) = xyzN[2] * finv((L + 16)/116 - b / 200);
    return xyzVector;
}

static QMatrix1x3 XYZtosRGB(const QMatrix1x3 &xyz)
{
    static const float xyzTosRGBValues[9] = {3.2404542f, -1.5371385f, -0.4985314f,
                                             -0.9692660f, 1.8760108f, 0.0415560f,
                                             0.0556434f, -0.2040259f, 1.0572252f};
    static const QMatrix3x3 xyzToSRGB{xyzTosRGBValues};

    auto gcor = [](float x) -> float {return x > 0.0031308f ? (1.055f * (std::pow(x, 1/2.4f)) - 0.055f) : (12.92 * x); };

    auto rgbG = xyzToSRGB * xyz;
    rgbG(0,0) = gcor(rgbG(0,0));
    rgbG(1,0) = gcor(rgbG(1,0));
    rgbG(2,0) = gcor(rgbG(2,0));

    return rgbG;
}

static QRgb averageColor(const QImage &image, const QRect &region)
{
    QMatrix1x3 labSum;
    labSum.fill(0);
    float alphaSum = 0;

    for(int y = region.top(); y <= region.bottom(); y++) {
        for(int x = region.left(); x <= region.right(); x++) {
            auto rgb = image.pixelColor(x,y);

            QMatrix1x3 rgbVector;
            rgbVector(0,0) = rgb.redF();
            rgbVector(1,0) = rgb.greenF();
            rgbVector(2,0) = rgb.blueF();
            alphaSum += rgb.alphaF();
            auto xyz = sRGBtoXYZ(rgbVector);
            auto lab = XYZtoLab(xyz);

            labSum += lab;
        }
    }

    int n = region.width() * region.height();
    auto avgLab = labSum / static_cast<float>(n);
    auto avgXYZ = LabToXYZ(avgLab);
    auto avgSrgb = XYZtosRGB(avgXYZ);
    auto avgAlpha = alphaSum / n;
    if(avgAlpha >= 0.5) {
        return QColor::fromRgbF(avgSrgb(0,0), avgSrgb(1,0), avgSrgb(2,0)).rgb();
    } else {
        return 0;
    }
}

PixelatedImage::PixelatedImage(const QImage &image, int pixelWidth, int pixelHeight)
{
    Q_ASSERT(pixelWidth > 0);
    Q_ASSERT(pixelHeight > 0);
    Q_ASSERT(image.valid(0,0));

    // Calculated the size of the image
    _size = QSize((image.width() + pixelWidth / 2) / pixelWidth,
        (image.height() + pixelHeight / 2) / pixelHeight);

    // Get the list of colors in the image
    std::vector<QRgb> imageColors;
    imageColors.reserve(_size.width() * _size.height());
    for(int y = 0; y < _size.height(); y++) {
        for(int x = 0; x < _size.width(); x++) {
            int w = image.width() - x * pixelWidth;
            int h = image.height() - y * pixelHeight;
            if(w > pixelWidth) w = pixelWidth;
            if(h > pixelHeight) h = pixelHeight;
            auto color = averageColor(image, QRect(x * pixelWidth, y * pixelHeight, w, h));
            imageColors.push_back(color);
        }
    }

    // Extract the unique colors
    _data.reserve(imageColors.size());
    std::unordered_map<QRgb, int> colorIndices;

    // Add the transparent color
    _colors.push_back(0);
    colorIndices[0] = 0;

    for(auto rgb : imageColors) {
        auto colorIt = colorIndices.find(rgb);
        if(colorIt != colorIndices.end()) {
            _data.push_back(colorIt->second);
        } else {
            int idx = static_cast<int>(_colors.size());
            _colors.push_back(rgb);
            colorIndices[rgb] = idx;
            _data.push_back(idx);
        }
    }

}

PixelatedImage::PixelatedImage(decltype (_data) data, decltype(_colors) colors, decltype (_size) size) :
    _data(std::move(data)), _colors(std::move(colors)), _size(std::move(size))
{

}

QColor PixelatedImage::getPixelColor(int x, int y) const
{
    Q_ASSERT(x >= 0 && x < _size.width());
    Q_ASSERT(y >= 0 && y < _size.height());

    int idx = _data[y * _size.width() + x];
    auto color = _colors[idx];
    return QColor::fromRgb(color);
}

int PixelatedImage::getPixelIdx(int x, int y) const
{
    Q_ASSERT(x >= 0 && x < _size.width());
    Q_ASSERT(y >= 0 && y < _size.height());

    return _data[y * _size.width() + x];
}

QImage PixelatedImage::asImage() const
{
    QImage image(_size, QImage::Format_RGBA8888);
    for(int y = 0; y < _size.height(); y++)
    {
        for(int x = 0; x < _size.width(); x++) {
            int idx = _data[y * _size.width() + x];
            auto color = _colors[idx];
            image.setPixelColor(x,y, QColor::fromRgba(color));
        }
    }
    return image;
}

void PixelatedImage::reduce(int numColors)
{
    Q_ASSERT(numColors > 0);
    Q_ASSERT(numColors <= static_cast<int>(_colors.size()));

    // Convert to Lab color space
    // First color is transparent
    std::vector<QMatrix1x3> inColors(_colors.size() - 1);
    std::transform(std::execution::par, _colors.begin() + 1, _colors.end(), inColors.begin(), [](QRgb rgb) { return sRGBtoLab(rgb); });

    // Generate initial means
    std::vector<QMatrix1x3> means;
    std::sample(inColors.begin(), inColors.end(), std::back_inserter(means), numColors, std::mt19937{std::random_device{}()});

    auto calcDis = [](const QMatrix1x3 &a, const QMatrix1x3 &b) ->float {
        float d1 = a(0,0) - b(0,0);
        float d2 = a(1,0) - b(1,0);
        float d3 = a(2,0) - b(2,0);
        float res = d1*d1 + d2*d2 + d3*d3;
        Q_ASSERT(res >= 0);
        return res;
    };

    // Assign sample to closest mean
    std::vector<std::vector<int>> clusters(numColors);
    auto assignSamples = [&inColors, &calcDis, &numColors, &means] (std::vector<std::vector<int>> &clusters) {
        for(int i = 0; i < static_cast<int>(inColors.size()); i++) {
            int nearestMean = 0;
            auto &sample = inColors[i];
            float nearestDistance = calcDis(means[nearestMean], sample);
            for(int j = 1; j < numColors; j++) {
                float distance = calcDis(means[j], sample);
                if(distance < nearestDistance) {
                    nearestMean = j;
                    nearestDistance = distance;
                }
            }
            clusters[nearestMean].push_back(i);
        }
    };
    assignSamples(clusters);

    // Improve the clusters
    for(int iterations = 0; iterations < 1000; iterations++)
    {
        // Recalculate mean
        std::transform(std::execution::par, clusters.begin(), clusters.end(), means.begin(), [&](const std::vector<int> &cluster){
            if(cluster.size() == 0) {
                QMatrix1x3 mean;
                std::sample(inColors.begin(), inColors.end(), &mean, 1, std::mt19937{std::random_device{}()});
                return mean;
            } else {
                QMatrix1x3 labSum;
                labSum.fill(0);

                for(int idx : cluster) {
                    labSum += inColors[idx];
                }

                return labSum / static_cast<float>(cluster.size());
            }
        });

        // Reassign samples
        std::vector<std::vector<int>> newClusters(numColors);
        assignSamples(newClusters);

        if(newClusters == clusters) {
            qDebug() << "Minimum after " << iterations << " iterations";
            break;
        }
        clusters = std::move(newClusters);
    }

    // Create new mapping from old colors to new colors
    // First color is transparent
    std::vector<int> remapping(inColors.size()+1);
    remapping[0] = 0;
    for(int j = 0; j < static_cast<int>(clusters.size()); j++) {
        for(auto i : clusters[j]) {
            remapping[i+1] = j + 1;
        }
    }

    // Recreate the image data
    for(int i = 0; i < static_cast<int>(_data.size()); i++) {
        _data[i] = remapping[_data[i]];
    }

    // Create the new colors
    // First color is transparent
    _colors.clear();
    _colors.push_back(0);
    for(int i = 0; i < numColors; i++) {
        auto color = XYZtosRGB(LabToXYZ(means[i]));
        _colors.push_back(QColor::fromRgbF(color(0,0), color(1,0), color(2,0)).rgb());
    }
}

PixelatedImage PixelatedImage::fromFile(const QString &path)
{
    using colorsType = decltype(PixelatedImage::_colors);
    using dataType = decltype(PixelatedImage::_data);
    QFile f(path);
    if(!f.open(QIODevice::ReadOnly)) {
        return PixelatedImage();
    }

    QDataStream strm(&f);

    // Write the file header
    quint32 magic;
    quint8 version;
    strm >> magic >> version;
    if(magic != 0x50494D2A) {
        return PixelatedImage();
    }

    strm.setVersion(QDataStream::Qt_5_14);

    // read in the image header
    QSize size;
    strm >> size;
    // Read in the image data
    dataType::size_type dataSize;
    strm >> dataSize;
    if(static_cast<int>(dataSize) != size.width() * size.height()) {
        return PixelatedImage();
    }
    dataType data;
    data.reserve(dataSize);
    for(dataType::size_type i = 0; i < dataSize; i++) {
        dataType::value_type v;
        strm >> v;
        data.push_back(v);
    }

    // Read in the color data
    colorsType::size_type colorsSize;
    strm >> colorsSize;
    colorsType colors;
    colors.reserve(colorsSize);
    for(colorsType::size_type i = 0; i < colorsSize; i++) {
        colorsType::value_type v;
        strm >> v;
        colors.push_back(v);
    }

    return PixelatedImage(std::move(data), std::move(colors), std::move(size));
}

bool PixelatedImage::save(const QString &path) const
{
    QFile f(path);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        return false;
    }

    QDataStream strm(&f);

    // Write the file header
    strm << (quint32)0x50494D2A;
    strm << (quint8)0x00; // version

    strm.setVersion(QDataStream::Qt_5_14);

    // write the image data
    strm << _size;
    strm << _data.size();
    for(auto elem : _data) {
        strm << elem;
    }
    strm << _colors.size();
    for(auto elem : _colors) {
        strm << elem;
    }

    return true;
}
