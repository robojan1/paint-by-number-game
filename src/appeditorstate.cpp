#include "appeditorstate.h"
#include <QtConcurrent/QtConcurrent>
#include "downloadmanager.h"
#include <appstate.h>

void AppEditorState::registerQml()
{
    qmlRegisterUncreatableType<AppEditorState>("App", 1, 0, "AppEditorState", tr("The state is an internal object"));
}

AppEditorState::AppEditorState(AppState *parent) : QObject(parent), QQuickImageProvider(QQuickImageProvider::Image),
    _downloadWatcher(nullptr), _appState(parent)
{
    connect(this, &AppEditorState::activeImageChanged, this, &AppEditorState::changed);
    connect(this, &AppEditorState::pixelatedImageChanged, this, &AppEditorState::changed);
    connect(this, &AppEditorState::imagesLoaded, this, &AppEditorState::changed);
    connect(&_downloadWatcher, &QFutureWatcher<DownloadResult>::finished, this, &AppEditorState::onFileDownloaded);
    connect(&_downloadWatcher, &QFutureWatcher<DownloadResult>::progressValueChanged, this, &AppEditorState::onFileDownloadProgress);
}

QImage AppEditorState::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QImage srcImg;
    if(id == "source" && !_activeImage.isNull()) {
        srcImg = _activeImage;
    } else if (id == "pixelated" && _pixelatedImage.isValid()){
        srcImg = _pixelatedImage.asImage();
    } else {
        srcImg = QImage(QSize(1,1), QImage::Format_RGBA8888);
        srcImg.setPixelColor(0,0, QColor::fromRgba(0));
    }
    if(size) {
        *size = srcImg.size();
    }
    if(requestedSize.isValid()) {
        srcImg.scaled(requestedSize);
    }
    return srcImg;
}

void AppEditorState::setActiveImage(QImage img)
{
    _activeImage = img;
    activeImageChanged();
}

void AppEditorState::loadImage(QUrl uri)
{
    if(!uri.isValid()) {
        emit imageLoadingProgress(LoadingState::Failed, 1.0, tr("Invalid link"));
        return;
    }

    emit imageLoadingProgress(LoadingState::Loading, 0.0, tr("Loading"));



    if(uri.isLocalFile()) {
        QString path = uri.toLocalFile();
        loadImageFromFile(path);
    } else {
        // Download the file
        auto manager = DownloadManager::instance();

        auto result = manager->download(uri);
        _downloadWatcher.setFuture(result);

    }
}

void AppEditorState::loadImageFromFile(QString path, QString mimeType)
{
    const char *format = nullptr;
    if(!mimeType.isEmpty()) {
        format = mimeType.toLocal8Bit().data();
    }
    QImage img(path, format);
    emit imageLoadingProgress(LoadingState::Loading, 1.0, tr("Loading"));

    if(img.isNull()) {
        emit imageLoadingProgress(LoadingState::Failed, 1.0, tr("Invalid image"));
    }
    setActiveImage(img);
    emit imageLoadingProgress(LoadingState::Done, 1.0, tr("Success"));
}

void AppEditorState::loadImageFromDB(QString id)
{
    auto info = (*_appState->imageDatabase())[id];
    if(info == nullptr) {
        return;
    }
    _activeImage = info->previewImage(false);
    _pixelatedImage = info->pixelatedImage(false);
    emit imagesLoaded();
}

void AppEditorState::generatePixelatedImage(int pixelSize, int numColors)
{
    if(_activeImage.isNull()) {
        return;
    }
    QtConcurrent::run(this, &AppEditorState::generatePixelatedImageInBackground,
                      pixelSize, numColors);
}

void AppEditorState::generatePixelatedImageInBackground(int pixelSize, int numColors)
{
    PixelatedImage newImage(_activeImage, pixelSize, pixelSize);
    newImage.reduce(numColors);
    _pixelatedImage = newImage;
    emit pixelatedImageChanged();
}

void AppEditorState::onFileDownloaded()
{
    auto result = _downloadWatcher.result();
    if(result.isSuccessful()) {
        loadImageFromFile(result.path(), result.mimetype());
        QFile::remove(result.path());
    } else {
        emit imageLoadingProgress(LoadingState::Failed, 1.0, result.msg());
    }
}

void AppEditorState::onFileDownloadProgress(int value)
{
    float maxValue = _downloadWatcher.progressMaximum();
    float progress;
    if(maxValue == 0) {
        progress = value != 0 ? 0.9f : 0.0f;
    } else {
        progress = value / maxValue * 0.9f;
    }
    emit imageLoadingProgress(LoadingState::Loading, progress, tr("Loading"));
}

void AppEditorState::saveImage(QString name)
{
    auto info = new ImageInfo(name, _activeImage, _pixelatedImage);
    _appState->imageDatabase()->push_back(info);
}

void AppEditorState::removeFromDB(QString id)
{
    _appState->imageDatabase()->remove(id);
}
