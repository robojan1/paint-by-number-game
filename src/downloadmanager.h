#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QFuture>
#include <QFile>
#include <QNetworkReply>

class DownloadResult {
public:
    DownloadResult(bool success, QString msgPath) : _path(msgPath), _success(success) {}

    bool isSuccessful() const { return _success; }
    QString path() const { return _path; }
    QString msg() const { return _path; }
    QString mimetype() const { return _mimetype; }

    void setMimeType(QString type) { _mimetype = type; }

private:
    QString _path;
    QString _mimetype;
    bool _success;
};

class DownloadManager : public QObject
{
    Q_OBJECT

    struct DownloadStatus {
        QNetworkReply *reply;
        QIODevice *file;
        QFutureInterface<DownloadResult> futureInterface;
        QString path;
        bool autoClose;
        bool deleteOnFail;
        bool failed;
    };

    static DownloadManager *_instance;
    QNetworkAccessManager *_nm;
    QHash<QNetworkReply *, DownloadStatus> _status;
public:
    static DownloadManager *instance() {
        if(_instance == nullptr) {
            _instance = new DownloadManager();
        }
        return _instance;
    }

    QFuture<DownloadResult> download(QUrl url);
    QFuture<DownloadResult> download(QUrl url, QString outputFile);
    QFuture<DownloadResult> download(QUrl url, QString path, QIODevice *outputFile, bool autoClose = true, bool deleteOnFail = true);

signals:


private:
    explicit DownloadManager(QObject *parent = nullptr);

    bool writeAvailableData(DownloadStatus *status);

private slots:
    void onRequestFinished();
    void onRequestProgress(qint64 bytesReceived, qint64 bytesTotal);
    void onRequestError(QNetworkReply::NetworkError code);
    void onRequestReadyRead();


};

#endif // DOWNLOADMANAGER_H
