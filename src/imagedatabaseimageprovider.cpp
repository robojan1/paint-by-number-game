#include "imagedatabaseimageprovider.h"
#include "imagedatabase.h"

ImageDatabaseImageProvider::ImageDatabaseImageProvider(const ImageDatabase *imgdb) :
     QQuickImageProvider(QQuickImageProvider::Image), _db(imgdb)
{

}

QImage ImageDatabaseImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    return _db->requestImage(id, size, requestedSize);
}
