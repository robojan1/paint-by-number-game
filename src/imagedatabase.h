#ifndef IMAGEDATABASE_H
#define IMAGEDATABASE_H

#include <QObject>
#include <QAbstractListModel>
#include <QVector>
#include "imageinfo.h"

class ImageDatabase : public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(Roles);

    QVector<ImageInfo *> _data;
    QHash<QString, ImageInfo *> _mapping;
public:

    enum Roles {
        DataRole = Qt::UserRole + 1,
    };

    ImageDatabase();
    ~ImageDatabase();

    void loadDatabase();
    void saveDatabase();

    virtual void push_back(ImageInfo * info);
    void remove(QString id);

    ImageInfo *operator[](const QString &id);

    Q_INVOKABLE QHash<int, QByteArray> roleNames() const final;

    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const final;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const final;

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) const;

private:
    void loadImages(const QJsonArray &list);
    void saveImages(QJsonArray &list);
};

#endif // IMAGEDATABASE_H
