import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Dialogs.qml 1.0
import App 1.0

Popup {
    id: element
    modal: true
    focus: true
    closePolicy: Popup.NoAutoClose
    horizontalPadding: 0
    verticalPadding: 0

    function progressUpdate(state, progress, message)
    {
        console.log("onProgressUpdate " + state + " " + progress + " " + message)
        if(state === AppEditorState.Failed || state === AppEditorState.Done) {
            completeButton.enabled = true;
        } else {
            completeButton.enabled = false;
        }
        progressBar.value = progress;
        if(state === AppEditorState.Failed) {
            progressText.text = qsTr("Error:\n%1").arg(message);
        } else {
            progressText.text = qsTr("Loading %1%:\n%2").arg(Math.round(progress * 100)).arg(message);
        }
    }

    onAboutToShow: {
        progressUpdate(AppEditorState.Loading, 0, "")
    }

    background: Rectangle {
        color: "lightgray"
        anchors.fill: parent
    }
    contentItem: Item {
        implicitHeight: titleBar.implicitHeight + content.implicitHeight + 30
        implicitWidth: 300
        Rectangle {
            id: titleBar
            color: "gray"
            anchors.top: element.top
            anchors.left: element.left
            anchors.right: element.right
            implicitHeight: title.implicitHeight + 10
            implicitWidth: element.width


            Label {
                id: title
                color: "#a9effc"
                text: qsTr("Loading image")
                font.pointSize: 14
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

            }
        }

        Item {
            id: content
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottom: element.bottom
            anchors.topMargin: 10
            anchors.top: titleBar.bottom
            anchors.left: element.left
            anchors.right: element.right
            implicitWidth: element.width
            implicitHeight: contentColumn.implicitHeight

            Column {
                id: contentColumn
                width: parent.width - 30
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 15


                Label {
                    id: progressText
                    text: qsTr("Loading...\n")
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pointSize: 12
                }

                ProgressBar {
                    id: progressBar
                    anchors.right: parent.right
                    anchors.rightMargin: 30
                    anchors.left: parent.left
                    anchors.leftMargin: 30
                }

                Button {
                    id: completeButton
                    text: qsTr("Ok")
                    anchors.horizontalCenter: parent.horizontalCenter
                    enabled: false
                    onClicked: element.close()
                }
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:150;width:300}
}
##^##*/
