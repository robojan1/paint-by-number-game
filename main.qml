import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.13
import App 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Paint by numbers game")

    StackView {
        id: mainStackView
        anchors.fill: parent
        initialItem: main_menu_view

    }

    Component {
        id: main_menu_view

        MainMenu {
            id: main_menu
            onAddImageButtonClicked: mainStackView.push(add_image_view)
        }
    }

    Component {
        id: add_image_view

        AddImageWindow {
            id: add_image_window
            onBackButtonClicked: mainStackView.pop()
            app: App.editorState
        }
    }

}

/*##^##
Designer {
    D{i:1;anchors_height:200;anchors_width:200}
}
##^##*/
