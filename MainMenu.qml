import QtQuick 2.0
import QtQuick.Controls 2.14

Item {
    id: window

    signal addImageButtonClicked

    Column {
        id: column
        Button {
            id: add_image_button
            text: qsTr("Add Image")
            onClicked: addImageButtonClicked()
        }
        anchors.fill: parent
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
