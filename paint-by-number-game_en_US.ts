<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="paint-by-number-game_en_US">
<context>
    <name>AddImageWindow</name>
    <message>
        <location filename="AddImageWindow.qml" line="35"/>
        <source>Please choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="129"/>
        <source>Add Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="138"/>
        <source>Block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="158"/>
        <source>Number of colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="178"/>
        <location filename="AddImageWindow.qml" line="188"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="194"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="208"/>
        <source>Go Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="AddImageWindow.qml" line="303"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppEditorState</name>
    <message>
        <location filename="src/appeditorstate.cpp" line="8"/>
        <source>The state is an internal object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/appeditorstate.cpp" line="50"/>
        <source>Invalid link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/appeditorstate.cpp" line="54"/>
        <location filename="src/appeditorstate.cpp" line="78"/>
        <location filename="src/appeditorstate.cpp" line="135"/>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/appeditorstate.cpp" line="81"/>
        <source>Invalid image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/appeditorstate.cpp" line="84"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppState</name>
    <message>
        <location filename="src/appstate.cpp" line="6"/>
        <source>The state is an internal object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadManager</name>
    <message>
        <location filename="src/downloadmanager.cpp" line="43"/>
        <source>Could not open output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/downloadmanager.cpp" line="101"/>
        <location filename="src/downloadmanager.cpp" line="145"/>
        <source>Could not write to file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageDatabase</name>
    <message>
        <location filename="src/imagedatabase.cpp" line="31"/>
        <source>No image database available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="37"/>
        <source>Could not read image database: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="45"/>
        <source>Image database format error:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="50"/>
        <source>Image database should be a json object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="57"/>
        <source>Missing required sections in the image database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="63"/>
        <source>Invalid object type in image database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="77"/>
        <source>Image is of the wrong type in the image database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="120"/>
        <source>Could not create the image database directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="128"/>
        <source>Could not open image database file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imagedatabase.cpp" line="136"/>
        <source>Error writing to the image database: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageInfo</name>
    <message>
        <location filename="src/imageinfo.cpp" line="28"/>
        <source>Image &quot;%1&quot; is missing some files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="34"/>
        <source>Could not read image info: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="42"/>
        <source>Image info format error:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="47"/>
        <source>Image info should be a json object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="53"/>
        <source>Missing required sections in the image info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="59"/>
        <source>id should be of type string in the image info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="64"/>
        <source>name should be of type string in the image info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="82"/>
        <source>Could not create the image database directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="93"/>
        <source>Error loading image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="99"/>
        <source>Could not copy the preview image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="107"/>
        <location filename="src/imageinfo.cpp" line="153"/>
        <location filename="src/imageinfo.cpp" line="170"/>
        <source>Error loading pixelated image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="113"/>
        <source>Could not save the pixelated image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="130"/>
        <source>Could not open image info file for writing: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/imageinfo.cpp" line="138"/>
        <source>Error writing to the image database: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageLoadingPopup</name>
    <message>
        <location filename="ImageLoadingPopup.qml" line="25"/>
        <source>Error:
%1</source>
        <oldsource>Failed:
%1</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ImageLoadingPopup.qml" line="27"/>
        <source>Loading %1%:
%2</source>
        <oldsource>Loading %1%:
	%2</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ImageLoadingPopup.qml" line="55"/>
        <source>Loading image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ImageLoadingPopup.qml" line="88"/>
        <source>Loading...
</source>
        <oldsource>Loading... </oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ImageLoadingPopup.qml" line="103"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="MainMenu.qml" line="13"/>
        <source>Add Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="10"/>
        <source>Paint by numbers game</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
