import QtQuick 2.0
import QtQuick.Controls 2.13
import QtQuick.Dialogs 1.2
import App 1.0

Item {
    id: window

    property var app

    signal backButtonClicked

    Connections {
        target: app
        onActiveImageChanged: {
            app.generatePixelatedImage(pixelSize.value, numColors.value)
        }
    }
    Connections {
        target: app
        onPixelatedImageChanged: {
            console.log("Reloading preview image")
            previewImage.source = ""
            previewImage.source = "image://editor/pixelated"
        }
        onImagesLoaded: {
            console.log("Reloading preview image")
            previewImage.source = ""
            previewImage.source = "image://editor/pixelated"
        }
    }

    FileDialog {
        id: openImageDialog
        title: qsTr("Please choose a file")
        folder: shortcuts.home
        selectMultiple: false
        selectFolder: false
        onAccepted: {
            app.loadImage(fileUrl)
        }
    }

    ImageLoadingPopup {
        id: loadingPopup
        anchors.centerIn: window
        Connections {
            target: app
            onImageLoadingProgress: {
                loadingPopup.progressUpdate(state, progress, message)
            }
        }
    }

    Page {
        anchors.bottom: images_box.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottomMargin: 0
        anchors.rightMargin: sidebar.width

        DropArea {
            id: droparea
            anchors.fill: parent
            keys: ['text/uri-list', 'image/jpeg', 'image/png', 'image/gif', 'image/bmp']
            onDropped: {
                if (drop.hasUrls) {
                    let uri = drop.urls[0]
                    console.log('loading url ' + uri)
                    loadingPopup.open()
                    app.loadImage(uri)
                } else {
                    console.log('dropped image data')
                }
            }
        }

        Image {
            id: previewImage
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: "image://editor/pixelated"
            smooth: false
            cache: false
        }
    }

    Item {
        id: sidebar
        width: 200
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        Rectangle {
            id: sidebarControls
            width: 200
            color: "#9e824c"
            radius: 8
            border.width: 8
            border.color: "#f3b718"
            anchors.bottom: preview_box.top
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0

            Column {
                id: sidebar_layout
                spacing: 2
                anchors.rightMargin: 10
                anchors.leftMargin: 10
                anchors.bottomMargin: 10
                anchors.topMargin: 10
                anchors.fill: parent

                Button {
                    id: add_image_button
                    height: 30
                    text: qsTr("Add Image")
                    topPadding: 6
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        openImageDialog.open()
                    }
                }

                Label {
                    text: qsTr("Block size")
                    font.pointSize: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                SpinBox {
                    id: pixelSize
                    height: 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    editable: true
                    value: 16
                    to: 128
                    from: 1
                    onValueChanged: {
                        app.generatePixelatedImage(pixelSize.value,
                                                   numColors.value)
                    }
                }

                Label {
                    text: qsTr("Number of colors")
                    font.pointSize: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                SpinBox {
                    id: numColors
                    height: 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    editable: true
                    value: 10
                    to: 500
                    from: 1
                    onValueChanged: {
                        app.generatePixelatedImage(pixelSize.value,
                                                   numColors.value)
                    }
                }

                Label {
                    text: qsTr("Name")
                    font.pointSize: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                TextField {
                    id: nameField
                    width: 150
                    height: 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    placeholderText: qsTr("Name")
                }

                Button {
                    id: saveButton
                    height: 30
                    text: qsTr("Save")
                    anchors.horizontalCenter: parent.horizontalCenter
                    enabled: app.imageGenerated && nameField.text.length > 0
                    onClicked: {
                        if(nameField.text.length == 0) {
                            return;
                        }
                        app.saveImage(nameField.text);
                    }
                }

                Button {
                    id: back_button
                    height: 30
                    text: qsTr("Go Back")
                    flat: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: window.backButtonClicked()
                }



            }
        }

        Rectangle {
            id: preview_box
            height: 200
            width: 200
            color: "#9e824c"
            radius: 8
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            border.width: 8
            border.color: "#f3b718"

            Image {
                id: sourceImage
                anchors.rightMargin: 10
                anchors.leftMargin: 10
                anchors.bottomMargin: 10
                anchors.topMargin: 10
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "image://editor/source"
                cache: false

                Connections {
                    target: app
                    onActiveImageChanged: {
                        console.log("Reloading source image")
                        sourceImage.source = ""
                        sourceImage.source = "image://editor/source"
                    }
                    onImagesLoaded: {
                        console.log("Reloading source image")
                        sourceImage.source = ""
                        sourceImage.source = "image://editor/source"
                    }
                }
            }
        }

    }

    Rectangle {
        id: images_box
        height: 100
        color: "#9e824c"
        radius: 8
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: sidebar.left
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.width: 8
        border.color: "#f3b718"

        ListView {
            id: imagesListPreview
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent
            height: parent.height
            model: App.imageDatabase
            orientation: ListView.Horizontal
            spacing: 10
            clip: true

            delegate: ImagePreviewButton {
                height: imagesListPreview.height
                width: height
                info: model.data
                onClicked: {
                    if(mouse.button === Qt.LeftButton) {
                        app.loadImageFromDB(model.data.uuid.toString())
                    }
                    else if(mouse.button === Qt.RightButton) {
                        contextMenu.popup(mouse.x, mouse.y)
                    }
                }
                Menu {
                    id: contextMenu
                    MenuItem {
                        text: qsTr("Delete")
                        onClicked: {
                            app.removeFromDB(model.data.uuid.toString())
                        }
                    }
                }
            }

        }

    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:25;anchors_height:200;anchors_width:200;anchors_x:18;anchors_y:20}
}
##^##*/

