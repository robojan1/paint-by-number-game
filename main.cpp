#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <pixelatedimage.h>
#include <QDebug>
#include <QQmlContext>
#include <appstate.h>
#include <downloadmanager.h>
#include <standardpaths.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Robbert-Jan");
    QCoreApplication::setOrganizationDomain("robojan.nl");
    QCoreApplication::setApplicationName("Paint by numbers");
    QCoreApplication::setApplicationVersion("0.1");

    QGuiApplication app(argc, argv);
    AppState state;

    QQmlApplicationEngine engine;

    // Register QML types
    AppState::registerQml();

    DownloadManager::instance()->setParent(&engine);

    qmlRegisterSingletonInstance("App", 1, 0, "App", &state);
    qmlRegisterSingletonInstance("App", 1, 0, "StandardPaths", StandardPaths::instance());

    engine.addImageProvider("editor", state.editorState());
    engine.addImageProvider("imgdb", state.imageDatabaseImageProvider());

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
